# Intro
This started as a university project to evolve a feeling for a search engine (based on elastic search). I decided to provide my application logic via the Python Flask framework. At this point, the project was done, however I did not stop yet. 

A few months later I started studying docker on my own and came back to this application and fully dockerized it. Then, I learned about docker-compose and came up with an even better way to containerize my application. In case anybody will ever try this application out I hope I didn't miss any configuration step. However, most of this application was built just for fun (only a minor part was actually required for the university project). I scored 96% in the project.

# Setup NEW NEW (with docker-compose)
- Run my whole application simply via `docker-compose up`
- Look at the section [Elastic Search Configuration](https://gitlab.com/fw93/elastic-search-project#elastic-search-configuration)

# Setup NEW (with Docker :)
- Get the elasticsearch docker image via `docker pull elasticsearch:7.4.0`
- Create a docker network via `docker network create my-network`
- Run the elasticsearch container via `docker run -d --name elasticsearch --net my-network -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.4.0`
- Build the docker image for my application using my Dockerfile via `docker build . -t flask-es`
- Run the container with my image via `docker run -d -p 5000:5000 --name flask --link elasticsearch:elasticsearch --net my-network flask-es`
- Look at the section [Elastic Search Configuration](https://gitlab.com/fw93/elastic-search-project#elastic-search-configuration)

# Elastic Search Configuration
An elastic search instance needs to run on `localhost:9200`. Then, our index `movies` can be created via `POST localhost:5000/elastic/index` (no body required). Next, the long web scraping process can be triggered via `POST localhost:5000/elastic` (without any body). You can follow the scraping process in the command line which runs our API because we have extensive logging. Note, that you can cancel the process at any time since the last scraped index is stored in `last_movie_scraped_index.txt`. However, you possibly lose the last 49 scraped movies since movies are written to elastic search in bulks of 50. To check, how many movies have actually been stored to elastic search already, call `POST http://localhost:9200/movies/_search?scroll=1m` (once again without any body).

# How to Use
Go to `localhost:5000` to see our minimalistic user interface. 

# Setup Old
## Installation (for native Linux OR Ubuntu Subsystem on Windows 10) 
If a current version of `python3` and `pip` is installed, all that's left to do is `sudo pip install pipenv`
which will install pipenv globally on your machine. Next you have to cd into this project folder and type in 
`pipenv shell --3.6` (**IMPORTANT**: The 3.6 needs to be **your** python3 version. Find it out via `python3 --version`) which creates a virtual environment (via python3) and **also activates it**. Next, type in `pipenv install` to install
all required dependencies. This could probably take like 1 minute. Then you are good to go.

## How to Run
You need to activate the virtual environment (if it is not activated yet) via `pipenv shell`. Then, the api has to be started
via `python3 src/main.py`.
