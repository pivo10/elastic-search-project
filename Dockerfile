FROM ubuntu

# update index and install dependencies
RUN apt-get update
RUN apt-get install -y locales python3.6 python3-pip
RUN pip3 install pipenv

# copy my application into the docker container
COPY . /opt/elasticsearch-project

# set the locale
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8   

# set working directory
WORKDIR /opt/elasticsearch-project

# # create virtual environment and install python dependencies
# RUN pipenv shell --3.6
# RUN pipenv install

RUN pip3 install flask
RUN pip3 install elasticsearch
RUN pip3 install pandas
RUN pip3 install xlrd
RUN pip3 install beautifulsoup4
RUN pip3 install requests

# command to execute python3 on the docker container (with the python file that is provided)
ENTRYPOINT ["python3"]

# default file to run if none specified
CMD ["/opt/elasticsearch-project/src/main.py"]